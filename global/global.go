package global

import (
	"ginmode/config"
	"ginmode/utils/timer"
	"ginmode/utils/translate"
	"github.com/go-redis/redis/v8"
	"github.com/songzhibin97/gkit/cache/local_cache"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"golang.org/x/sync/singleflight"
	"gorm.io/gorm"
)

var (
	GVA_DB                  *gorm.DB
	GVA_DBList              map[string]*gorm.DB
	GVA_REDIS               *redis.Client
	GVA_CONFIG              config.Server
	GVA_VP                  *viper.Viper
	GVA_LOG                 *zap.Logger
	GVA_Timer               timer.Timer          = timer.NewTimerTask()
	GVA_TRANSLATOR          translate.Translator // support multilanguage
	GVA_Concurrency_Control = &singleflight.Group{}

	BlackCache local_cache.Cache
)

// Translate support multilanguage
func Translate(msg string) string {
	if GVA_TRANSLATOR.IsInit {
		message := GVA_TRANSLATOR.TranslateMessage(msg)
		return message
	}
	return msg
}
