package translate

import (
	"encoding/json"
	"fmt"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
	"io/fs"
	"os"
	"sort"
)

type Translator struct {
	IsInit    bool // check if translator initialized or not
	bundle    *i18n.Bundle
	localizer *i18n.Localizer
}

// func (t *Translator) InitTranslator(initLang string) {
// 	// support multilanguage
// 	t.bundle = i18n.NewBundle(language.English)
// 	t.bundle.RegisterUnmarshalFunc("json", json.Unmarshal)
// 	t.bundle.MustLoadMessageFile("./lang/en.json")
// 	t.bundle.MustLoadMessageFile("./lang/zh.json")
// 	t.bundle.MustLoadMessageFile("./lang/ar.json")
// 	t.localizer = i18n.NewLocalizer(t.bundle, initLang) // should add additionl check here
// 	t.IsInit = true
// 	// end of adding
// }

// ReadDir 读取文件
func ReadDir(dirname string) ([]fs.FileInfo, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	list, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return nil, err
	}
	sort.Slice(list, func(i, j int) bool { return list[i].Name() < list[j].Name() })
	return list, nil
}

// InitTranslator support multilanguage
func (t *Translator) InitTranslator(initLang string, langPath string) {
	langFiles, err := ReadDir(langPath)
	if err != nil {
		fmt.Printf("InitTranslator() Error: %v", err)
	}

	t.bundle = i18n.NewBundle(language.English)
	t.bundle.RegisterUnmarshalFunc("json", json.Unmarshal)

	for _, langFile := range langFiles {
		if !langFile.IsDir() {
			langFilePath := langPath + langFile.Name()
			fmt.Printf("Language file: %s loaded.\r\n", langFilePath)
			t.bundle.MustLoadMessageFile(langFilePath)
		}
	}

	t.localizer = i18n.NewLocalizer(t.bundle, initLang) // should add additionl check here
	t.IsInit = true
	// end of adding
}

func (t *Translator) SetTranslatorLanguage(lang string) {
	t.localizer = i18n.NewLocalizer(t.bundle, lang)
}

func (t *Translator) TranslateMessage(messageID string) string {
	translatedMsg, err := t.localizer.LocalizeMessage(&i18n.Message{ID: messageID})
	if err != nil {
		return messageID
	}
	//return t.localizer.MustLocalize(&i18n.LocalizeConfig{MessageID: messageID})
	return translatedMsg
}
