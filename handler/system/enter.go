package system

import "ginmode/service"

type ApiGroup struct {
	DBApi
	SystemApi
}

var (
	systemConfigService = service.ServiceGroupApp.SystemServiceGroup.SystemConfigService
	initDBService       = service.ServiceGroupApp.SystemServiceGroup.InitDBService
)
