package initialize

import (
	"ginmode/global"
	"ginmode/initialize/internal"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func GormSqlite() *gorm.DB {
	m := global.GVA_CONFIG.Sqlite
	if m.Dbname == "" {
		return nil
	}
	if db, err := gorm.Open(sqlite.Open(m.Dsn()), internal.Gorm.Config(m.Prefix, m.Singular)); err != nil {
		return nil
	} else {
		//db.InstanceSet("gorm:table_options", "ENGINE="+m.Engine)
		sqlDB, _ := db.DB()
		sqlDB.SetMaxIdleConns(m.MaxIdleConns)
		sqlDB.SetMaxOpenConns(m.MaxOpenConns)
		return db
	}
}
